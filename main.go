package main

import (
	"strings"

	"github.com/leaanthony/mewn"
	"github.com/mitchellh/mapstructure"
	"github.com/wailsapp/wails"
)

func Hello(m []interface{}) string {
	var s []string
	err := mapstructure.Decode(m, &s)
	if err != nil {
		return "ERROR: " + err.Error()
	}
	return "Hello " + strings.Join(s, ", ") + "!"
}

func main() {
	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:  640,
		Height: 640,
		Title:  "Sample app",
		JS:     js,
		CSS:    css,
		Colour: "#FFFFFF",
	})
	app.Bind(Hello)
	app.Run()
}
