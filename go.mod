module wails-vue-sample

go 1.14

require (
	github.com/leaanthony/mewn v0.10.7
	github.com/mitchellh/mapstructure v1.3.3
	github.com/wailsapp/wails v1.8.0
)
